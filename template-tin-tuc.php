<?php
/*
Template Name: Xem tin tức
Template Post Type: post, page, event
*/
// Page code here...

?>


<!DOCTYPE html>
<html lang="vi">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="//img.zing.vn/products/vltkm/favicon.ico" />
    <meta name="robots" content="index,follow" />
    <meta name="revisit-after" content="1days" />
    <title>Xem tin tức</title>
    <meta name="description"
        content="Kiếm Thế - Game Kiếm Hiệp do VNG phát hành với cộng đồng lớn nhất Việt Nam, quy tụ 13 môn phái, chiến trường PK rộng lớn, event, tính năng đa dạng... | Kiếm Thế - Game Kiếm Hiệp do VNG phát hành với cộng đồng lớn nhất Việt Nam, quy tụ 13 môn phái, chiến trường PK rộng lớn, event, tính năng đa dạng..." />
    <meta name="keywords" content="Kiếm Thế private - kiếm thế mới ra - kiếm thế 2009 - kiếm thế VNG" />
    <meta name="google-site-verification" content="xPSgBSfWXd5bk31ICazGl1WvpG3B1J4j5Cg9P6V8OZo" />
    <meta property="og:title" content="Kiếm Thế - Game Kiếm Hiệp do VNG phát hành với cộng đồng lớn nhất Việt Nam," />
    <meta property="og:description" content="Kiếm Thế Hoàng Gia" />
    <meta property="og:url" content="<?php get_home_url(); ?>" />
    <!-- End Facebook Pixel Code -->
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
    <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/style.css" />
    <!-- <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/skin-subpage-v4.css" /> -->
</head>

<body class="subpage">
    <!-- menu-new -->
    <div class="nav-top setPosTop hide">
        <ul class="flex main-mobile">
            <li>
                <a href="/" class="logo"><img src="<?php echo get_template_directory_uri() ?>/images/logo.png"
                        class="mr-2"></a>
            </li>
            <li class="right"> <a class="napthe-mb" href="http://kiemthetinhkiem.com/tich-luy-nap-the"><span
                        class="hidden">napthe</span></a>
            </li>
            <li> <a class="home-mb" href="/"><span class="hidden">homemb</span></a>
            </li>
            <li> <a class="fb-mb" href="http://www.facebook.com/kiemthetinhkiem"><span class="hidden">fb</span></a>
            </li>
            <li> <a class="open-menu" href="#"><span class="hidden">homemb</span></a>
            </li>
        </ul>
        <!-- desktop -->

        <ul class="menu flex">
        <li class="flex center"> <a class="flex" href="/" title="Trang chủ"><span class="fontdow">Trang chủ</span></a>
         </li>
            <li class="flex center"> <a class="flex" data-active-path="" href="http://kiemthetinhkiem.com/cai-dat-game"
                    title="Tải game ngay"><span>Tải Game
                        Ngay</span></a>
            </li>
            <li class="flex center"> <a class="flex" data-active-path=""
                    href="http://kiemthetinhkiem.com/taikhoan/login.html" title="Đăng nhập"><span>Đăng nhập</span></a>
            </li>
            <li class="flex center"> <a class="flex" data-active-path=""
                    href="http://kiemthetinhkiem.com/taikhoan/register.html" title="Đăng ký"><span>Đăng ký</span></a>
            </li>
            <li class="flex center"> <a class="flex" data-active-path=""
                    href="http://kiemthetinhkiem.com/tich-luy-nap-the/" title="Nạp thẻ"><span>Nạp thẻ</span></a>
            </li>
            <li class="flex center"> <a class="flex" data-active-path=""
                    href="http://kiemthetinhkiem.com/huong-dan-tan-thu/" title="Hướng dẫn"><span>Hướng dẫn</span></a>
            </li>

            <li class="flex center"> <a class="flex" data-active-path="" href="http://www.facebook.com/kiemthetinhkiem"
                    title="Tin tức"><span>Cộng đồng</span></a>
            </li>
        </ul>
    </div>
    <!-- end -->
    <div class="scroll-content">
        <!-- <div class="desktop placeholder-top"></div> -->
        <!-- .section-1 -->
        <section class="section section-1-page" id="header">
            <div class="section__background">
            <img src="<?php echo get_template_directory_uri() ?>/images/banner/bg-subpage.jpg" alt="" class="desktop">
            </div>
            <a class="logo" href="/"><img src="<?php echo get_template_directory_uri() ?>/images/logo.png"></a>
        </section>
        <!-- end -->
        <section class="section section--content wapper-content">

            <div class="main">
                <div class="section__background-2">
                    <img src="<?php echo get_template_directory_uri() ?>/images/banner/main-content-bg.png" alt=""
                        class="desktop">
                </div>
                <div class="main__content sub">
                <div class="title-sub-top flex center"><img alt="" src="<?php echo get_template_directory_uri() ?>/images/banner/title-news.png"></div>
                    <div class="main-page-single" id="MainContent">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="main-content-article">
                                    <!-- <a title="Đầu trang" class="Top" id="top" href="#header">Đầu trang</a> -->
                                    <div class="main-content--bot clr-white">
                                        <div class="title-sub">
                                            <ul class="posts__tab-2 flex" id="posts__tab-2">
                                                <!-- <li>
                                                    <a href="/" class="flex center"><span>Trang
                                                            chủ</span></a>
                                                </li> -->
                                                <li><a href="http://kiemthetinhkiem.com/thong-bao/"
                                                        class="flex center"><span>Thông
                                                            báo</span></a></li>
                                                <li><a href="http://kiemthetinhkiem.com/tin-tuc/"
                                                        class="flex center active"><span>Tin
                                                            tức</span></a></li>
                                                <li><a href="http://kiemthetinhkiem.com/su-kien/"
                                                        class="flex center"><span>Sự
                                                            kiện</span></a></li>
                                                <li><a href="http://kiemthetinhkiem.com/cam-nang/"
                                                        class="flex center"><span>Cẩm
                                                            nang</span></a></li>
                                            </ul>
                                        </div>
                                        <!-- end -->
                                        <!-- <div class="img-content">
                                            <img width="100%" alt="" class="img-titile"
                                                src="<?php echo get_template_directory_uri() ?>/images/banner/title-news.png">
                                        </div> -->
                                        <article>
                                            <div id="posts__list_tintuc" class="posts__list">
                                                <ul>

                                                    <?php
                                                    $the_query = new WP_Query(
                                                        array(
                                                            'category_name' => 'tin-tuc',
                                                            'posts_per_page' => 100
                                                        )
                                                    );

                                                    // The Loop
                                                    if ($the_query->have_posts()) {

                                                        while ($the_query->have_posts()) {
                                                            $the_query->the_post();

                                                            $category_detail = get_the_category(get_the_ID()); //$post->ID
                                                            foreach ($category_detail as $cd) {
                                                                $catename = $cd->cat_name;
                                                            }
                                                            ?>
                                                            <li>
                                                                <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                                                                    <span class="flex">
                                                                        <!-- <span class='posts__post-cate news fontdow'>
                                                        <?php echo $catename ?>
                                                    </span> -->
                                                    <span class="posts__post-cate news">Tin Tức</span>
                                                                        <span class="posts__post-title">
                                                                            <?php the_title() ?>
                                                                        </span>
                                                                    </span>

                                                                    <span class="posts__post-date">
                                                                        <?php echo get_the_date('d-m-Y') ?>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            <?php
                                                        }
                                                    } else {
                                                        // no posts found
                                                    }
                                                    ?>

                                                </ul>
                                            </div>
                                            <!-- end -->
                                        </article>
                                    </div>
                                </div>
                            </div>
                            <!-- end -->
                        </div>
                    </div>
                    <!-- end -->
                </div>
            </div>
        </section>
        <!-- end -->
        <section class="section section--footer flex">
            <div class="copyright container">
                <div class="d-flex align-items-center justify-content-center">
                    <a href="/" class="logo-footer"><img
                            src="<?php echo get_template_directory_uri() ?>/images/logo.png" class="mr-4"></a>
                    <p class="text clr-white fz-18"> Mọi thắc mắc vui lòng liên hệ: <a
                            href="http://www.facebook.com/kiemthetinhkiem" class="clr-white text-underline">Fanpage Kiếm
                            Thế Hoàng Gia</a><br>
                        <span class="mt-2">Kiếm Thế Tình Kiếm mới ra - Hoàn toàn miễn phí - Cày cuốc hấp dẫn - Chuẩn Hồi
                            ức
                            2009.</span>
                    </p>
                </div>
            </div>
        </section>
    </div>
    <!-- end -->
    <div class="fix-icon-top">
        <a title="Đầu trang" class="Top" id="top" href="#header">Đầu trang</a>
    </div>
    <div id="thewindowbackground"></div>
    <div class="nav_right open">
        <ul class="buttai">
            <li class="app-info__install--pc button-fanpage">
                <a target="_blank" href="http://www.facebook.com/kiemthetinhkiem" title="fanpage"></a>
            </li>
            <li class="app-info__install--pc button-group">
                <a target="_blank" href="http://www.facebook.com/kiemthetinhkiem" title="group"></a>
            </li>
        </ul>
        <a class="gift_code" title="Tham gia group để nhận VIPcode"></a>
        <span class="i_control"></span>
    </div>
    <!-- end -->
    <script type="text/javascript">
        var domain = "//vltkm.zing.vn";
        var domain2 = "//vltkm.zing.vn";
    </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/popper.min.js"></script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
    <!-- <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/swiper-3.4.2.min.js"></script> -->
    <!-- <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script> -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/customs.js"></script>
    <!-- <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/skin-homepage-v4.js"></script> -->
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/skin-subpage-v3.js"></script>
    </script>
    <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/skin-homepage-v4.js"></script>

</body>

</html>