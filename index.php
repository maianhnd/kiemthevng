<!DOCTYPE html>
<html lang="vi">

<head>
   <meta charset="utf-8" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <!-- <meta name="viewport" content="width=device-width, initial-scale=1.0"> -->
   <!-- <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> -->
   <link rel="shortcut icon" href="//img.zing.vn/products/vltkm/favicon.ico" />
   <meta name="google-site-verification" content="42X1PvTdy7gk2eHYCsEN05jqwn05U66Shu_xaGtIXn0" />
   <meta name="robots" content="index,follow" />
   <meta name="revisit-after" content="1days" />
   <meta name="facebook-domain-verification" content="zpb09bpdwq6wr35361khhkauh5vu1a" />
   <title>
      <?php echo get_bloginfo('name') ?>
   </title>
   <meta name="description" content="Kiếm Thế Hoàng gia - Tái hiện hồi ức vang bóng một thời" />
   <meta name="keywords" content="Kiếm Thế Hoàng gia" />
   <meta property="og:title" content="Kiếm Thế Hoàng gia - Tinh hoa dòng game kiếm hiệp nhập vai" />
   <meta property="og:description" content="Kiếm Thế Hoàng gia" />
   <meta property="og:url" content="<?php get_home_url() ?>" />
   <meta property="og:image" content="//img.zing.vn/products/vltkm/FBShare3.jpg" />
   <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
   <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" />
   <!-- <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/skin-homepage-v5.css" /> -->
   <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/style.css" />

   <!-- Meta Pixel Code -->
   <script>
      !function (f, b, e, v, n, t, s) {
         if (f.fbq) return; n = f.fbq = function () {
            n.callMethod ?
               n.callMethod.apply(n, arguments) : n.queue.push(arguments)
         };
         if (!f._fbq) f._fbq = n; n.push = n; n.loaded = !0; n.version = '2.0';
         n.queue = []; t = b.createElement(e); t.async = !0;
         t.src = v; s = b.getElementsByTagName(e)[0];
         s.parentNode.insertBefore(t, s)
      }(window, document, 'script',
         'https://connect.facebook.net/en_US/fbevents.js');
      fbq('init', '1462818330831138');
      fbq('track', 'PageView');
   </script>
   <noscript><img height="1" width="1" style="display:none"
         src="https://www.facebook.com/tr?id=1462818330831138&ev=PageView&noscript=1" /></noscript>
   <!-- End Meta Pixel Code -->


   <!-- Global site tag (gtag.js) - Google Analytics -->
   <script async src="https://www.googletagmanager.com/gtag/js?id=G-FBRLPP0PX2"></script>
   <script>
      window.dataLayer = window.dataLayer || [];
      function gtag() { dataLayer.push(arguments); }
      gtag('js', new Date());

      gtag('config', 'G-FBRLPP0PX2');
   </script>
</head>

<body>
   <!-- <div id="topbar" style="display: none; height: 30px"></div> -->
   <!-- <div class="wrapper-out"> -->

   <!-- menu-new -->
   <div class="nav-top bg-nav-1 setPosTop hide">
      <ul class="flex main-mobile">
         <li>
            <!-- <a class="app-icon-mb" href="/"></a> -->
            <a href="/" class="logo"><img src="<?php echo get_template_directory_uri() ?>/images/logo.png"
                  class="mr-2"></a>
         </li>
         <li class="right"> <a class="napthe-mb" href="http://kiemthetinhkiem.com/tich-luy-nap-the"><span class="hidden">napthe</span></a>
         </li>
         <li> <a class="home-mb" href="/"><span class="hidden">homemb</span></a>
         </li>
         <li> <a class="fb-mb" href="http://www.facebook.com/kiemthetinhkiem"><span class="hidden">fb</span></a>
         </li>
         <li> <a class="open-menu" href="#"><span class="hidden">homemb</span></a>
         </li>
      </ul>
      <!-- desktop -->

      <ul class="menu flex">
         <li class="flex center"> <a class="flex" data-active-path="" href="http://kiemthetinhkiem.com/cai-dat-game"
               title="Tải game ngay"><span class="fontdow">Tải Game
                  Ngay</span></a>
         </li>
         <li class="flex center"> <a class="flex" data-active-path=""
               href="http://kiemthetinhkiem.com/taikhoan/login.html" title="Đăng nhập"><span>Đăng nhập</span></a>
         </li>
         <li class="flex center"> <a class="flex" data-active-path=""
               href="http://kiemthetinhkiem.com/taikhoan/register.html" title="Đăng ký"><span>Đăng ký</span></a>
         </li>
         <li class="flex center"> <a class="flex" data-active-path=""
               href="http://kiemthetinhkiem.com/tich-luy-nap-the/" title="Nạp thẻ"><span>Nạp thẻ</span></a>
         </li>
         <li class="flex center"> <a class="flex" data-active-path=""
               href="http://kiemthetinhkiem.com/huong-dan-tan-thu/" title="Hướng dẫn"><span>Hướng dẫn</span></a>
         </li>

         <li class="flex center"> <a class="flex" data-active-path="" href="http://www.facebook.com/kiemthetinhkiem"
               title="Tin tức"><span>Cộng đồng</span></a>
         </li>
      </ul>
   </div>
   <!-- end -->
   <!-- body -->
   <div class="wrapper">
      <div class="main-wrap">
         <!-- ./banner -->
         <!-- <section class="section section-banner">
                        
                  </section> -->
         <div class="banner-head section conL">
            <!-- <a href="/" target="_blank" title="Kiếm Thế Hoàng gia" class="logo"></a> -->
            <!-- <img src="images/banner/head.jpg" alt=""> -->
            <div class="head-video">
               <video src="<?php echo get_template_directory_uri() ?>/images/video/video1.mp4" autoplay="" muted=""
                  class="video1"></video>
               <video src="<?php echo get_template_directory_uri() ?>/images/video/video2.mp4" autoplay="" muted=""
                  loop="" class="video2"></video>
            </div>

         </div>
         <!-- end -->
         <div class="content-wapper section">
            <!-- section-2 -->
            <section class="section section-2">
               <div class="scrolldown">
                  <span class="hidden">scrolldown</span>
               </div>
               <div class="f2-title text-center"><img
                     src="<?php echo get_template_directory_uri() ?>/images/banner/f2-title.png" class="mx-auto"
                     alt="Võ lầm truyền kỳ - 20 năm 1 hành trình">
               </div>
               <div class="content-news">
                  <div class="banner">
                     <div class="swiper-container banner-swiper">
                        <div class="swiper-wrapper">
                           <div class="swiper-slide">
                              <div class="banner-frame flex center">
                                 <a href="http://kiemthetinhkiem.com/nhiem-vu-thuong-hoi/" target="_blank">
                                    <img class="img-banner lazyloaded" alt="OPEN SEVER TRUYỀN KỲ"
                                       data-src="<?php echo get_template_directory_uri() ?>/images/upload/opentk.png"
                                       src="<?php echo get_template_directory_uri() ?>/images/upload/opentk.png">
                                 </a>
                              </div>
                           </div>
                           <div class="swiper-slide">
                              <div class="banner-frame flex center">
                                 <a href="http://kiemthetinhkiem.com/danh-sach-hoat-dong/" target="_blank">
                                    <img class="img-banner lazyloaded" alt="LỊCH HOẠT ĐỘNG HẰNG NGÀY"
                                       data-src="<?php echo get_template_directory_uri() ?>/images/upload/lichhoatdong.png"
                                       src="<?php echo get_template_directory_uri() ?>/images/upload/lichhoatdong.png">
                                 </a>
                              </div>
                           </div>
                           <div class="swiper-slide">
                              <div class="banner-frame flex center">
                                 <a href="http://kiemthetinhkiem.com/huong-dan-tan-thu/" target="_blank">
                                    <img class="img-banner lazyloaded" alt="HƯỚNG DẪN TÂN THỦ"
                                       data-src="<?php echo get_template_directory_uri() ?>/images/upload/huongdantanthu.png"
                                       src="<?php echo get_template_directory_uri() ?>/images/upload/huongdantanthu.png">
                                 </a>
                              </div>
                           </div>
                           <div class="swiper-slide">
                              <div class="banner-frame flex center">
                                 <a href="http://kiemthetinhkiem.com/tich-luy-nap-the/" target="_blank">
                                    <img class="img-banner lazyloaded" alt="CODE NẠP THẺ VÀ CODE KHUYẾN MÃI"
                                       data-src="<?php echo get_template_directory_uri() ?>/images/upload/kmthenap.png"
                                       src="<?php echo get_template_directory_uri() ?>/images/upload/kmthenap.png">
                                 </a>
                              </div>
                           </div>
                        </div>
                        <!-- Add Pagination --><!-- Add Arrows -->
                        <!-- <div class="swiper-button-next"></div>
                           <div class="swiper-button-prev"></div> -->
                        <div class="swiper-pagination"></div>
                     </div>
                  </div>
                  <!-- end -->
                  <div class="posts">
                     <ul class="posts__tab flex" id="posts__tab">
                        <!-- <li><a href="#" data-block-name="home-news" data-category=""
                                    data-shorturl="posts__list_tinhnang" class="flex center">Thông báo</a></li> -->
                        <li><a href="#" data-block-name="home-news" data-category="" data-shorturl="posts__list_tintuc"
                              class="flex center active">Tin tức</a></li>
                        <li><a href="#" data-block-name="home-event" data-category="" data-shorturl="posts__list_sukien"
                              class="flex center">Sự kiện</a></li>
                        <li><a href="#" data-block-name="home-news" data-category=""
                              data-shorturl="posts__list_huongdan" class="flex center">Cẩm nang</a></li>
                     </ul>
                     <!-- <a class="posts__view" id="posts__view-all" href="#" title="Xem thêm thông báo"
                        data-block-name="home-news" data-category="" data-shorturl="posts__list_tinhnang">&nbsp</a> -->
                     <a class="posts__view" id="posts__view-all" href="http://kiemthetinhkiem.com/thong-bao/"
                        title="Xem thêm thông báo">&nbsp</a>

                     <!-- Tính năng -->
                     <div id="posts__list_tinhnang">
                        <ul class="posts__list">
                           <?php
                           $the_query = new WP_Query(
                              array(
                                 'category_name' => 'thong-bao',
                                 'posts_per_page' => 6
                              )
                           );

                           // The Loop
                           if ($the_query->have_posts()) {

                              while ($the_query->have_posts()) {
                                 $the_query->the_post();
                                 ?>
                                 <li><a class="posts__post-title" href="<?php the_permalink() ?>"
                                       title="<?php the_title() ?>"><span>
                                          <?php the_title() ?>
                                       </span><time datetime="<?php echo get_the_date('d-m-Y') ?>"><?php echo get_the_date('d-m-Y') ?></time></a></li>
                                 <?php
                              }
                           } else {
                              // no posts found
                           }
                           ?>
                        </ul>
                     </div>
                     <!-- End Tính năng -->
                     <!-- Tin tức -->
                     <div id="posts__list_tintuc" style="display:none;">
                        <ul class="posts__list">
                           <?php
                           $the_query = new WP_Query(
                              array(
                                 'category_name' => 'tin-tuc',
                                 'posts_per_page' => 6
                              )
                           );

                           // The Loop
                           if ($the_query->have_posts()) {

                              while ($the_query->have_posts()) {
                                 $the_query->the_post();
                                 ?>
                                 <li><a class="posts__post-title" href="<?php the_permalink() ?>"
                                       title="<?php the_title() ?>"><span>
                                          <?php the_title() ?>
                                       </span><time datetime="<?php echo get_the_date('d-m-Y') ?>"><?php echo get_the_date('d-m-Y') ?></time></a></li>
                                 <?php
                              }
                           } else {
                              // no posts found
                           }
                           ?>
                        </ul>
                     </div>
                     <!-- End Tin Tức -->
                     <!-- Sự kiện -->
                     <div id="posts__list_sukien" style="display:none;">
                        <ul class="posts__list">
                           <?php
                           $the_query = new WP_Query(
                              array(
                                 'category_name' => 'su-kien',
                                 'posts_per_page' => 6
                              )
                           );

                           // The Loop
                           if ($the_query->have_posts()) {

                              while ($the_query->have_posts()) {
                                 $the_query->the_post();
                                 ?>
                                 <li><a class="posts__post-title" href="<?php the_permalink() ?>"
                                       title="<?php the_title() ?>"><span>
                                          <?php the_title() ?>
                                       </span><time datetime="<?php echo get_the_date('d-m-Y') ?>"><?php echo get_the_date('d-m-Y') ?></time></a></li>
                                 <?php
                              }
                           } else {
                              // no posts found
                           }
                           ?>
                        </ul>
                     </div>
                     <!-- End Sự Kiện -->
                     <!-- Hướng dẫn -->
                     <div id="posts__list_huongdan" style="display:none;">
                        <ul class="posts__list">
                           <?php
                           $the_query = new WP_Query(
                              array(
                                 'category_name' => 'cam-nang',
                                 'posts_per_page' => 6
                              )
                           );

                           // The Loop
                           if ($the_query->have_posts()) {

                              while ($the_query->have_posts()) {
                                 $the_query->the_post();
                                 ?>
                                 <li><a class="posts__post-title" href="<?php the_permalink() ?>"
                                       title="<?php the_title() ?>"><span>
                                          <?php the_title() ?>
                                       </span><time datetime="<?php echo get_the_date('d-m-Y') ?>"><?php echo get_the_date('d-m-Y') ?></time></a></li>
                                 <?php
                              }
                           } else {
                              // no posts found
                           }
                           ?>
                        </ul>
                     </div>
                     <!-- End Hướng dẫn -->
                  </div>
               </div>
            </section>
            <!-- end -->
            <section class="section section-3">
               <div class="f4-title text-center"><img
                     src="<?php echo get_template_directory_uri() ?>/images/banner/f4-title.png" class="mx-auto"
                     alt="Tính Năng Hoạt Động"></div>
               <div class="newsbox">
                  <div class="max-height-news">
                     <div class="row">
                        <div class="col-4">
                           <div class="block-list-function">
                              <ul>
                                 <li>
                                    <a href="http://kiemthetinhkiem.com/che-tao-trang-bi/" title="Chế tạo Trang bị">Chế
                                       tạo Trang bị</a>
                                 </li>
                                 <!-- end -->

                                 <li>
                                    <a href="http://kiemthetinhkiem.com/he-thong-tien-te" title="Hệ thống Tiền Tệ">Hệ
                                       thống Tiền Tệ</a>
                                 </li>
                                 <!-- end -->

                                 <li><a href="http://kiemthetinhkiem.com/ngu-hanh-an/" title="Ngũ Hành Ấn">Ngũ hành ấn</a></li>
                                 <!-- end -->

                                 <li><a href="http://kiemthetinhkiem.com/nhiem-vu-truy-na/"
                                       title="Nhiệm Vụ Truy Nã">Nhiệm
                                       vụ Truy Nã</a></li>
                                 <!-- end -->

                                 <li><a href="http://kiemthetinhkiem.com/boss-vo-lam-cao-thu/" title="Võ Lâm Cao Thủ">Võ
                                       lâm Cao Thủ</a></li>
                                 <!-- end -->

                              </ul>
                           </div>

                        </div>
                        <div class="col-4">
                           <div class="block-list-function">
                              <ul>
                                 <li>
                                    <a href="http://kiemthetinhkiem.com/du-long-bi-bao/" title="Hệ thống Thú cưỡi">Du
                                       Long Bí Bảo</a>
                                 </li>
                                 <!-- end -->
                                 <li>
                                    <a href="http://kiemthetinhkiem.com/than-thu-hoa-ky-lan/" title="Hoa Ky Lan">Thần
                                       Thú Hỏa Kỳ Lân</a>
                                 </li>
                                 <!-- end -->
                                 <li><a href="http://kiemthetinhkiem.com/cuong-hoa-trang-bi/"
                                       title="Hệ thống Đồng Hành">Cường
                                       hóa Trang bị</a></li>
                                 <!-- end -->
                                 <li><a href="http://kiemthetinhkiem.com/nhiem-vu-thuong-hoi/"
                                       title="Nhiệm Vụ Thương Hội">Nhiệm vụ Thương Hội</a></li>
                                 <!-- end -->
                                 <li><a href="http://kiemthetinhkiem.com/boss-kim-mao-su-vuong"
                                       title="Kim Mao Sư Vương">Kim
                                       Mao Sư Vương</a></li>
                                 <!-- end -->
                              </ul>
                           </div>
                        </div>
                        <div class="col-4">
                           <div class="block-list-function">
                              <ul>
                                 <li>
                                    <a href="http://kiemthetinhkiem.com/cai-dat-game/" title="Hệ thống Thú cưỡi">
                                    Tải game</a>
                                 </li>
                                 <!-- end -->
                                 <li>
                                    <a href="http://kiemthetinhkiem.com/huong-dan-auto-du-long/" title="Hoa Ky Lan">
                                    Hướng dẫn Auto Du Long</a>
                                 </li>
                                 <!-- end -->
                                 <li><a href="http://kiemthetinhkiem.com/boss-vo-lam-cao-thu/"
                                       title="Hệ thống Đồng Hành">Boss Kim Mao Sư Vương</a></li>
                                 <!-- end -->
                                 <li><a href="http://kiemthetinhkiem.com/nhiem-vu-thuong-hoi/"
                                       title="Nhiệm Vụ Thương Hội">Boss Võ Lâm Cao Thủ</a></li>
                                 <!-- end -->
                                 <li><a href="http://kiemthetinhkiem.com/huong-dan-tan-thu/"
                                       title="Kim Mao Sư Vương">Hướng dẫn tân thủ</a></li>
                                 <!-- end -->
                              </ul>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </section>

            <!-- //footer -->
            <footer class="footer-site">
               <div class="copyright">
                  <div class="text-left">
                     <div class="d-flex align-items-center justify-content-center">
                     <a href="/" class="logo-footer"><img src="<?php echo get_template_directory_uri() ?>/images/logo.png"
                              class="mr-4"></a>
                        <div class="text-right-footer">
                           <p class="text-uppercase fz-18 mb-2">Kiếm thế tình kiếm</p>
                           <p class="text clr-white fz-18"> Mọi thắc mắc vui lòng liên hệ: <a
                                 href="http://www.facebook.com/kiemthetinhkiem" class="clr-white text-underline">Fanpage
                                 Kiếm
                                 Thế Hoàng Gia</a>
                           </p>
                        </div>

                     </div>
                  </div>
               </div>
               <!-- <div class="copyright">
                  <div class="text-left">
                     <p class="text-uppercase fz-18 mb-2">Kiếm thế tình kiếm</p>
                     <a href="http://kiemthetinhkiem.com" class="fz-16 a-link">kiemthetinhkiem.com</a>
                     <p class="text fz-16 mt-2">Mọi thắc mắc vui lòng liên hệ: <a
                           href="http://www.facebook.com/kiemthetinhkiem" class="a-link">
                           Fanpage Kiếm Thế Hoàng gia
                        </a>
                     </p>
                  </div>
                

               </div> -->
            </footer>
         </div>

         <!-- end -->
      </div>
   </div>
   <!-- <a id="scrolltop" href="#" class="top"></a> -->
   <!-- </div> -->
   <!--<div class="boxfbchat"  style="position: fixed;bottom: 0;right: 0;z-index: 500;">
         <div class="box_hotro"><span class="text">Bạn cần hỗ trợ </span></div>
         <label for="navbar-toggle" class="close ">Menu</label><iframe src="https://www.facebook.com/plugins/page.php?href=https://www.facebook.com/messages/t/104092295734733" width="250" height="250" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
      </div>-->
   <div class="nav_right open">
      <ul class="buttai">
         <li class="app-info__install--pc button-fanpage">
            <a target="_blank" href="http://www.facebook.com/kiemthetinhkiem" title="fanpage"></a>
         </li>
         <li class="app-info__install--pc button-group">
            <a target="_blank" href="http://www.facebook.com/kiemthetinhkiem" title="group"></a>
         </li>
      </ul>
      <a class="gift_code" title="Tham gia group để nhận VIPcode"></a>
      <span class="i_control"></span>
   </div>
   <div id="thewindowbackground"></div>
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/popper.min.js"></script>
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
   <!-- <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/swiper-3.4.2.min.js"></script> -->
   <!-- <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script> -->
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/customs.js"></script>
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/skin-homepage-v4.js"></script>
   <!-- Initialize Swiper -->
   <!-- <script>
    var swiper = new Swiper(".mySwiper", {
      spaceBetween: 30,
      hashNavigation: {
        watchState: true,
      },
      pagination: {
        el: ".swiper-pagination",
        clickable: true,
      },
      
    });
  </script> -->

</body>

</html>