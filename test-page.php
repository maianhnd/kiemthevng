<!DOCTYPE html>
<html lang="vi">

<head>
   <meta charset="utf-8" />
   <meta http-equiv="X-UA-Compatible" content="IE=edge" />
   <link rel="shortcut icon" href="//img.zing.vn/products/vltkm/favicon.ico" />
   <meta name="robots" content="index,follow" />
   <meta name="revisit-after" content="1days" />
   <title>
      <?php single_post_title(); ?> - Kiếm Thế Hoàng gia
   </title>
   <meta name="description" content="Kiếm Thế Hoàng gia - Tái hiện hồi ức vang bóng một thời" />
   <meta name="keywords" content="Kiếm Thế private - kiếm thế mới ra - kiếm thế 2009 - kiếm thế VNG" />
   <meta property="og:title" content="Kiếm Thế Hoàng gia | Tinh hoa dòng game kiếm hiệp nhập vai" />
   <meta property="og:description" content="Kiếm Thế Hoàng gia" />
   <meta property="og:url" content="<?php get_home_url(); ?>" />
   <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@300&display=swap" rel="stylesheet">
   <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/bootstrap.min.css" />
   <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/style.css" />
   <!-- <link rel="stylesheet" media="screen" href="<?php echo get_template_directory_uri() ?>/css/skin-subpage-v4.css" /> -->
   <!--[if lte IE 9]><script type="text/javascript" src="//img.zing.vn/products/vltkm/skin-2016/js-full/lib/modernizr.js"></script>< ![endif]-->

</head>

<body class="subpage">
   <!-- menu-new -->
   <div class="nav-top bg-nav-1 setPosTop hide">
      <ul class="flex main-mobile">
         <li>
            <a class="app-icon-mb" href="/"></a>
         </li>
         <li class="right"> <a class="napthe-mb" href="#"><span class="hidden">napthe</span></a>
         </li>
         <li> <a class="home-mb" href="/"><span class="hidden">homemb</span></a>
         </li>
         <li> <a class="fb-mb" href="http://www.facebook.com/kiemthetinhkiem"><span class="hidden">fb</span></a>
         </li>
         <li> <a class="open-menu" href="#"><span class="hidden">homemb</span></a>
         </li>
      </ul>
      <!-- desktop -->

      <ul class="menu flex">
         <li class="flex center"> <a class="flex" data-active-path="" href="http://kiemthetinhkiem.com/cai-dat-game"
               title="Tải game ngay"><span class="fontdow">Tải Game
                  Ngay</span></a>
         </li>
         <li class="flex center"> <a class="flex" data-active-path=""
               href="http://kiemthetinhkiem.com/taikhoan/login.html" title="Đăng nhập"><span>Đăng nhập</span></a>
         </li>
         <li class="flex center"> <a class="flex" data-active-path=""
               href="http://kiemthetinhkiem.com/taikhoan/register.html" title="Đăng ký"><span>Đăng ký</span></a>
         </li>
         <li class="flex center"> <a class="flex" data-active-path=""
               href="http://kiemthetinhkiem.com/tich-luy-nap-the/" title="Nạp thẻ"><span>Nạp thẻ</span></a>
         </li>
         <li class="flex center"> <a class="flex" data-active-path=""
               href="http://kiemthetinhkiem.com/huong-dan-tan-thu/" title="Hướng dẫn"><span>Hướng dẫn</span></a>
         </li>

         <li class="flex center"> <a class="flex" data-active-path="" href="http://www.facebook.com/kiemthetinhkiem"
               title="Tin tức"><span>Cộng đồng</span></a>
         </li>
      </ul>
   </div>
   <!-- end -->
   <div class="wapper-content">
      <div class="banner-head section conL">
         <!-- <a href="/" target="_blank" title="Kiếm Thế Hoàng gia" class="logo"></a> -->
         <!-- <img src="images/banner/head.jpg" alt=""> -->
         <div class="head-video">
            <video src="<?php echo get_template_directory_uri() ?>/images/video/video1.mp4" autoplay="" muted=""
               class="video1"></video>
            <video src="<?php echo get_template_directory_uri() ?>/images/video/video2.mp4" autoplay="" muted="" loop=""
               class="video2"></video>
         </div>
      </div>
      <!-- end -->

      <div class="main__content sub">
         <div class="title-sub">
            <article>
               <ul class="posts__tab flex" id="posts__tab">
                  
                  <li><a href="#" class="flex center" data-target="danh-sach"
                        data-shorturl="posts__list_tinhnang"><span>Thông báo</span></a></li>
                  <li><a href="#" data-target="danh-sach" data-shorturl="posts__list_tintuc"
                        class="flex center"><span>Tin
                           tức</span></a></li>
                  <li><a href="#" data-target="tin-tuc" data-shorturl="posts__list_sukien" class="flex center"><span>Sự
                           kiện</span></a></li>
                  <li><a href="#" data-target="tinh-nang" data-shorturl="posts__list_huongdan"
                        class="flex center"><span>Cẩm
                           nang</span></a></li>
               </ul>
               <div id="posts__list_tintuc" class="posts__list" style="display:none;">
                  <ul>

                     <?php
                     $the_query = new WP_Query(
                        array(
                           'category_name' => 'tin-tuc',
                           'posts_per_page' => 100
                        )
                     );

                     // The Loop
                     if ($the_query->have_posts()) {

                        while ($the_query->have_posts()) {
                           $the_query->the_post();

                           $category_detail = get_the_category(get_the_ID()); //$post->ID
                           foreach ($category_detail as $cd) {
                              $catename = $cd->cat_name;
                           }
                           ?>
                           <li>
                              <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                                 <span class="flex">
                                    <span class='posts__post-cate news'>
                                       <?php echo $catename ?>
                                    </span>
                                    <span class="posts__post-title">
                                       <?php the_title() ?>
                                    </span>
                                 </span>

                                 <span class="posts__post-date">
                                    <?php echo get_the_date('d-m-Y') ?>
                                 </span>
                              </a>
                           </li>
                           <?php
                        }
                     } else {
                        // no posts found
                     }
                     ?>

                  </ul>
               </div>
               <!-- end -->
               <div id="posts__list_sukien" class="posts__list" style="display:none;">
                  <ul>

                     <?php
                     $the_query = new WP_Query(
                        array(
                           'category_name' => 'su-kien',
                           'posts_per_page' => 100
                        )
                     );

                     // The Loop
                     if ($the_query->have_posts()) {

                        while ($the_query->have_posts()) {
                           $the_query->the_post();

                           $category_detail = get_the_category(get_the_ID()); //$post->ID
                           foreach ($category_detail as $cd) {
                              $catename = $cd->cat_name;
                           }
                           ?>
                           <li>
                              <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                                 <span class="flex">
                                    <span class='posts__post-cate news'>
                                       <?php echo $catename ?>
                                    </span>
                                    <span class="posts__post-title">
                                       <?php the_title() ?>
                                    </span>
                                 </span>

                                 <span class="posts__post-date">
                                    <?php echo get_the_date('d-m-Y') ?>
                                 </span>
                              </a>
                           </li>
                           <?php
                        }
                     } else {
                        // no posts found
                     }
                     ?>
                  </ul>
               </div>
               <div id="posts__list_tinhnang" class="posts__list" style="display:none;">
                  <ul>
                     <?php
                     $the_query = new WP_Query(
                        array(
                           'category_name' => 'thong-bao',
                           'posts_per_page' => 100
                        )
                     );

                     // The Loop
                     if ($the_query->have_posts()) {

                        while ($the_query->have_posts()) {
                           $the_query->the_post();

                           $category_detail = get_the_category(get_the_ID()); //$post->ID
                           foreach ($category_detail as $cd) {
                              $catename = $cd->cat_name;
                           }
                           ?>
                           <li>
                              <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                                 <span class="flex">
                                    <span class='posts__post-cate news'>
                                       <?php echo $catename ?>
                                    </span>
                                    <span class="posts__post-title">
                                       <?php the_title() ?>
                                    </span>
                                 </span>

                                 <span class="posts__post-date">
                                    <?php echo get_the_date('d-m-Y') ?>
                                 </span>
                              </a>
                           </li>
                           <?php
                        }
                     } else {
                        // no posts found
                     }
                     ?>
                  </ul>
               </div>
               <div id="posts__list_huongdan" class="posts__list" style="display:none;">
                  <ul>

                     <?php
                     $the_query = new WP_Query(
                        array(
                           'category_name' => 'cam-nang',
                           'posts_per_page' => 100
                        )
                     );

                     // The Loop
                     if ($the_query->have_posts()) {

                        while ($the_query->have_posts()) {
                           $the_query->the_post();

                           $category_detail = get_the_category(get_the_ID()); //$post->ID
                           foreach ($category_detail as $cd) {
                              $catename = $cd->cat_name;
                           }
                           ?>
                           <li>
                              <a href="<?php the_permalink() ?>" title="<?php the_title() ?>">
                                 <span class="flex">
                                    <span class='posts__post-cate news'>
                                       <?php echo $catename ?>
                                    </span>
                                    <span class="posts__post-title">
                                       <?php the_title() ?>
                                    </span>
                                 </span>

                                 <span class="posts__post-date">
                                    <?php echo get_the_date('d-m-Y') ?>
                                 </span>
                              </a>
                           </li>
                           <?php
                        }
                     } else {
                        // no posts found
                     }
                     ?>
                  </ul>
               </div>
            </article>
         </div>
         <!-- end -->

         <!-- //content -->
         <div class="container">
            <div class="main-content hidden-tab-content" id="main-content">
               <div class="main-content--bot clr-white">
                  <a id="scrolltop" href="#" class="top"></a>
                  <div class="main-content__title">
                     <h1 class="text-center text-uppercase">
                     <?php
                     $category = get_the_category();
                     echo $firstCategory = $category[0]->cat_name;
                     ?>
                  </h1>
                     
                     <ul id="breadcrumb" class="breadcrumb">
                     <li itemprop="url" itemprop=&#039;child&#039; itemscope
                        itemtype="http://data-vocabulary.org/Breadcrumb"><a href="<?php get_home_url(); ?>" rel=""><span
                              itemprop="title">Trang chủ</span></a> &gt; </li>
                              <li class="active" itemprop=&#039;child&#039; itemscope itemtype="http:/data-vocabulary.org/Breadcrumb"><span itemprop="title"><?php single_post_title(); ?></span></li>
                  </ul>
                  </div>
                  <article>
                     <h2 class="article__title">
                        <?php the_title(); ?>
                     </h2>
                     <time class="article__time">Ngày:
                        <?php echo get_the_date('d-m-Y') ?>
                     </time>
                     <div class="article__detail">
                        <div class="StaticMain">
                           <div class="StaticMain" id="noidung">

                              <?php the_content(); ?>

                           </div>
                        </div>
                     </div>
                     <input type="hidden" id='article-cateCode' value="tinh-nang-dac-sac">
                  </article>
               </div>
            </div>
         </div>
      </div>


      <!-- end -->
   </div>

   <!-- end -->

   <div class="nav_right open">
      <ul class="buttai">
         <li class="app-info__install--pc button-fanpage">
            <a target="_blank" href="http://www.facebook.com/kiemthetinhkiem" title="fanpage"></a>
         </li>
         <li class="app-info__install--pc button-group">
            <a target="_blank" href="http://www.facebook.com/kiemthetinhkiem" title="group"></a>
         </li>
      </ul>
      <a class="gift_code" title="Tham gia group để nhận VIPcode"></a>
      <span class="i_control"></span>
   </div>
   <!-- end -->
   <div id="thewindowbackground"></div>
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/jquery.min.js"></script>
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/popper.min.js"></script>
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/bootstrap.min.js"></script>
   <!-- <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/swiper-3.4.2.min.js"></script> -->
   <!-- <script src="https://cdn.jsdelivr.net/npm/swiper@9/swiper-bundle.min.js"></script> -->
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/customs.js"></script>
   <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/skin-homepage-v4.js"></script>
   <!-- <script type="text/javascript" src="<?php echo get_template_directory_uri() ?>/js/skin-subpage-v3.js"></script> -->
   <!-- <script>
      var oddFilter = function (index) {
         console.log(index);
         return (index % 2) == 1;
      },
         evenFilter = function (index) {
            console.log(index);
            return (index % 2) == 0;
         }

      $('ul').filter(oddFilter).addClass('Icon').end()



   </script> -->

</body>

</html>